<?php

/**
 * Implements hook_install().
 *
 * Perform actions to set up the site for this profile.
 */
function btsa_profile_install() {
  $themes['theme_default'] = variable_get('btsa_theme_default', 'bartik');
  $themes['admin_theme'] = variable_get('btsa_admin_theme', 'seven');

  // Insert default pre-defined node types into the database. For a complete
  // list of available node type attributes, refer to the node type API
  // documentation at: http://api.drupal.org/api/HEAD/function/hook_node_info.
  $types = array(
    array(
      'type' => 'page',
      'name' => st('Basic page'),
      'base' => 'node_content',
      'description' => st("Use <em>basic pages</em> for your static content, such as an 'About us' page."),
      'custom' => 1,
      'modified' => 1,
      'locked' => 0,
    ),
  );

  foreach ($types as $type) {
    $type = node_type_set_defaults($type);
    node_type_save($type);
    node_add_body_field($type);
  }

  // Default "Basic page" to not be promoted and have comments disabled.
  variable_set('node_options_page', array('status'));

  // Don't display date and author information for "Basic page" nodes by default.
  variable_set('node_submitted_page', FALSE);

  _btsa_profile_setup_year_vocabulary();

  _btsa_profile_setup_module_vocabulary();

  _btsa_profile_setup_name_vocabulary();

  _btsa_profile_setup_themes($themes);

  _btsa_profile_setup_time_date();

  $filter_perms = _btsa_profile_setup_filter_formats();

  _btsa_profile_setup_menus();

  _btsa_profile_setup_blocks($themes);

  _btsa_profile_setup_roles($filter_perms);

  _btsa_profile_setup_home_page();
}

 function _btsa_profile_setup_year_vocabulary() {
     // Create a vocabulary named "BSTA Status".
    $description = st('The BTSA year of participating teacher.');
    $help = st('Select the year for this teacher.');
    $vocabulary = (object) array(
      'name' => st('FactDoc Year'),
      'description' => $description,
      'machine_name' => 'factdoc_year',
      'help' => $help,

    );
    taxonomy_vocabulary_save($vocabulary);

    $names = array(
      'Year 1',
      'Year 2',
      'ECO',
    );

    $weight = 0;
    foreach($names as $name) {
      $term = (object) array(
        'name' => $name,
        'vid' => $vocabulary->vid,
        'weight' => $weight,
      );
      taxonomy_term_save($term);
      $weight += 5;
      $terms[$name] = $term->tid;
    }
}

function _btsa_profile_setup_module_vocabulary() {
    // Create a vocabulary named "BSTA Modules".
    $description = st('BTSA Modules');
    $help = st('The module for each factdoc.');
    $vocabulary = (object) array(
      'name' => st('FactDoc Module'),
      'description' => $description,
      'machine_name' => 'factdoc_module',
      'help' => $help,

    );
    taxonomy_vocabulary_save($vocabulary);

    $names = array(
      'Module A - Context for Teaching and Learning',
      'Module B - Assessment of Teaching and Learning',
      'Module C - Inquiry into Teaching and Learning',
      'Module D - Summary of Teaching and Learning',
      'Module C - Inquiry into Teaching and Learning PT2',
    );

    $weight = 0;
    foreach($names as $name) {
      $term = (object) array(
        'name' => $name,
        'vid' => $vocabulary->vid,
        'weight' => $weight,
      );
      taxonomy_term_save($term);
      $weight += 5;
      $terms[$name] = $term->tid;
    }
}

function _btsa_profile_setup_name_vocabulary() {
    // Create a vocabulary named "FactDoc Names".
    $description = st('FactDoc Names');
    $help = st('The names for each factdoc.');
    $vocabulary = (object) array(
      'name' => st('FactDoc Names'),
      'description' => $description,
      'machine_name' => 'factdoc_name',
      'help' => $help,

    );
    taxonomy_vocabulary_save($vocabulary);

    $names = array(
      'A Context for Teaching and Learning',
      'A-1 Class Profile',
      'A-2 Classroom Layout',
      'A-3 School and District Information_Resources',
      'A-4 Home_School Communication',
      'A-5 Site Orientation Checklist',
      'A-6 Community Description',
      'B Assessment of Teaching and Learning Year 1',
      'B-3 K-W-O Chart',
      'B-5 Initial Classroom Observation',
      'B-6 Post-Observation Record',
      'C Inquiry into Teaching and Learning',
      'C-1 Individual Induction Plan',
      'C-2 Essential Components for Instruction',
      'C-3 Entry-Level Assessment Resource (Part 1 and 2)',
      'C-4 Focus Student Selection',
      'C-5 Lesson Plan Observation (for OB #1)',
      'C-5 Lesson Plan Observation (for OB #2)',
      'C-6 Inquiry Observation Record (for OB #1)',
      'C-6 Inquiry Observation Record (for OB#2)',
      'C-7 Analysis of Student Work (for OB #1)',
      'C-7 Analysis of Student Work (for OB #2)',
      'C-8 Summative Assessment',
      'E-2 Self-Assessment of CSTPs',
      'E-3 Self-Assessment of Pedagogy',
      'E-4 Self-Assessment of Universal Acess - Equity for All Students',
      'E-5 Self-Assessment of Universal Access-Teaching English Learners',
      'E-6 Self-Assessment of Universal Access- Teaching Special Populations',
      'D Summary of Teaching and Learning',
      'D-2 Culmination Questions- Written Reflection',
      'B Assessment of Teaching and Learning Year 2',
      'B-3 Pre-Observation Conference (OB#1)',
      'B-3 Pre-Observation Conference (OB#2)',
      'B-3 Pre-Observation Conference (OB#3)',
      'B-5 Classroom Observation #1',
      'B-5 Classroom Observation #2',
      'B-5 Classroom Observation #3',
      'B-6 Post-Observation Reflection (OB#1)',
      'B-6 Post-Observation Reflection (OB#2)',
      'B-6 Post-Observation Reflection (OB#3)',
    );

    $weight = 0;
    foreach($names as $name) {
      $term = (object) array(
        'name' => $name,
        'vid' => $vocabulary->vid,
        'weight' => $weight,
      );
      taxonomy_term_save($term);
      $weight += 5;
      $terms[$name] = $term->tid;
    }
}


function _btsa_profile_setup_themes($themes) {
  theme_enable($themes);

  foreach ($themes as $var => $theme) {
    if (!is_numeric($var)) {
      variable_set($var, $theme);
    }
  }

  // Disable bartik if it's not used
  if ($themes['theme_default'] !== 'bartik') {
    theme_disable(array('bartik'));
  }

  // Make the admin theme the default for creating content.
  variable_set('node_admin_theme', '1');
}

function _btsa_profile_setup_time_date() {
  // Set default country to US
  variable_set('site_default_country', 'US');
  // First day of the week will be sunday
  variable_set('date_first_day', '0');
  // Timezone
  variable_set('date_default_timezone', 'America/Los_Angeles');
  // Users set their own timezone
  variable_set('configurable_timezones', 0);
  // Don't show a message to users if the timezone is empty
  variable_set('empty_timezone_message', 0);
  // Users default timezone
  variable_set('user_default_timezone', '0');

  // Insert custom format: November 25, 2011 - 2:45pm
  db_insert('date_formats')
    ->fields(array('format' => 'F j, Y - g:ia', 'type' => 'custom', 'locked' => 0))
    ->execute();

  // Insert custom format: November 25, 2011
  db_insert('date_formats')
    ->fields(array('format' => 'F j, Y', 'type' => 'custom', 'locked' => 0))
    ->execute();

  // Insert custom format: Nov. 25, 2011 - 2:45pm
  db_insert('date_formats')
    ->fields(array('format' => 'M. j, Y - g:ia', 'type' => 'custom', 'locked' => 0))
    ->execute();

  // Insert custom format: Nov. 25, 2011
  db_insert('date_formats')
    ->fields(array('format' => 'M. j, Y', 'type' => 'custom', 'locked' => 0))
    ->execute();

  // Insert custom format: 11/25/2011
  db_insert('date_formats')
    ->fields(array('format' => 'm/d/Y', 'type' => 'custom', 'locked' => 0))
    ->execute();


  // Insert custom format: November 25, 2011 - 2:45pm
  db_insert('date_format_type')
    ->fields(array('type' => 'long_time', 'title' => 'Long With Time', 'locked' => 0))
    ->execute();

  // Insert custom format: Nov. 25, 2011 - 2:45pm
  db_insert('date_format_type')
    ->fields(array('type' => 'medium_time', 'title' => 'Medium With Time', 'locked' => 0))
    ->execute();

  // Insert custom format: 11/25/2011 - 2:45pm
  db_insert('date_format_type')
    ->fields(array('type' => 'short_time', 'title' => 'Short With Time', 'locked' => 0))
    ->execute();


  // Date Long Format
  variable_set('date_format_long_time', 'F j, Y - g:ia');
  variable_set('date_format_long', 'F j, Y');

  // Date Medium Format
  variable_set('date_format_medium_time', 'M. j, Y - g:ia');
  variable_set('date_format_medium', 'M. j, Y');

  // Date Short Format
  variable_set('date_format_short_time', 'm/d/Y - g:ia');
  variable_set('date_format_short', 'm/d/Y');
}

function _btsa_profile_setup_filter_formats() {
  // Add text formats.
  $filtered_html_format = array(
    'format' => 'filtered_html',
    'name' => 'Filtered HTML',
    'weight' => 0,
    'filters' => array(
      // URL filter.
      'filter_url' => array(
        'weight' => 0,
        'status' => 1,
      ),
      // HTML filter.
      'filter_html' => array(
        'weight' => 1,
        'status' => 1,
        'settings' => array(
          'allowed_html' => '<a> <em> <strong> <cite> <blockquote> <ul> <ol> <li> <dl> <dt> <dd> <img> <table> <tbody> <th> <tr> <td> <div>',
        ),
      ),
      // Line break filter.
      'filter_autop' => array(
        'weight' => 2,
        'status' => 1,
      ),
      // HTML corrector filter.
      'filter_htmlcorrector' => array(
        'weight' => 10,
        'status' => 1,
      ),
    ),
  );
  $filtered_html_format = (object) $filtered_html_format;
  filter_format_save($filtered_html_format);

  $full_html_format = array(
    'format' => 'full_html',
    'name' => 'Full HTML',
    'weight' => 1,
    'filters' => array(
      // URL filter.
      'filter_url' => array(
        'weight' => 0,
        'status' => 1,
      ),
      // Line break filter.
      'filter_autop' => array(
        'weight' => 1,
        'status' => 1,
      ),
      // HTML corrector filter.
      'filter_htmlcorrector' => array(
        'weight' => 10,
        'status' => 1,
      ),
    ),
  );
  $full_html_format = (object) $full_html_format;
  filter_format_save($full_html_format);

  // Enable default permissions for system roles.
  $filter_perms = array(
    'filtered_html' => filter_permission_name($filtered_html_format),
    'full_html' => filter_permission_name($full_html_format),
  );

  return $filter_perms;
}

function _btsa_profile_setup_menus() {
  // Build left footer menu
  $left_footer_definition = array(
    'menu_name' => 'menu-left-footer',
    'title' => 'Left Footer',
    'description' => 'This is the left footer menu.',
  );

  menu_save($left_footer_definition);

  // Build center footer menu
  $center_footer_definition = array(
    'menu_name' => 'menu-center-footer',
    'title' => 'Center Footer',
    'description' => 'This is the center footer menu.',
  );

  menu_save($center_footer_definition);

  // Build right footer menu
  $right_footer_definition = array(
    'menu_name' => 'menu-right-footer',
    'title' => 'Right Footer',
    'description' => 'This is the right footer menu.',
  );

  menu_save($right_footer_definition);
}

function _btsa_profile_setup_blocks($themes) {
  // Enable some standard blocks.
  $values = array(
    array(
      'module' => 'system',
      'delta' => 'main',
      'theme' => $themes['theme_default'],
      'status' => 1,
      'weight' => 0,
      'region' => 'content',
      'pages' => '',
      'cache' => -1,
    ),
    array(
      'module' => 'system',
      'delta' => 'help',
      'theme' => $themes['theme_default'],
      'status' => 1,
      'weight' => 0,
      'region' => 'help',
      'pages' => '',
      'cache' => -1,
    ),
    array(
      'module' => 'system',
      'delta' => 'navigation',
      'theme' => $themes['theme_default'],
      'status' => 1,
      'weight' => 0,
      'region' => 'sidebar_first',
      'pages' => '',
      'cache' => -1,
    ),
    array(
      'module' => 'system',
      'delta' => 'main',
      'theme' => $themes['admin_theme'],
      'status' => 1,
      'weight' => 0,
      'region' => 'content',
      'pages' => '',
      'cache' => -1,
    ),
    array(
      'module' => 'system',
      'delta' => 'help',
      'theme' => $themes['admin_theme'],
      'status' => 1,
      'weight' => 0,
      'region' => 'help',
      'pages' => '',
      'cache' => -1,
    ),
    array(
      'module' => 'user',
      'delta' => 'login',
      'theme' => $themes['admin_theme'],
      'status' => 1,
      'weight' => 10,
      'region' => 'content',
      'pages' => '',
      'cache' => -1,
    ),
    array(
      'module' => 'menu_block',
      'delta' => 'btsa_secondary_menus-1',
      'theme' => $themes['theme_default'],
      'status' => 1,
      'weight' => 0,
      'region' => 'sidebar_first',
      'pages' => '',
      'cache' => -1,
    ),
  );
  $query = db_insert('block')->fields(array('module', 'delta', 'theme', 'status', 'weight', 'region', 'pages', 'cache'));
  foreach ($values as $record) {
    $query->values($record);
  }
  $query->execute();
}

function _btsa_profile_setup_roles($filter_perms) {
  // Set permissions for anonymous and authenticated roles.
  user_role_grant_permissions(DRUPAL_ANONYMOUS_RID, array('access content', $filter_perms['filtered_html']));
  user_role_grant_permissions(DRUPAL_AUTHENTICATED_RID, array('access content', $filter_perms['filtered_html']));

  // Create a default role for site administrators, with all available permissions assigned.
  $admin_role = new stdClass();
  $admin_role->name = 'administrator';
  $admin_role->weight = 4;
  user_role_save($admin_role);
  user_role_grant_permissions($admin_role->rid, array_keys(module_invoke_all('permission')));
  // Set this as the administrator role.
  variable_set('user_admin_role', $admin_role->rid);

  // Assign user 1 the "administrator" role.
  db_insert('users_roles')
    ->fields(array('uid' => 1, 'rid' => $admin_role->rid))
    ->execute();

  // Create the participating teacher role
  $participating_teacher = new stdClass();
  $participating_teacher->name = 'participating teacher';
  $participating_teacher->weight = 2;
  user_role_save($participating_teacher);

  $participating_teacher_permissions = array(
    $filter_perms['filtered_html'],
//    'manage content workflow',
    'create fact_doc content',
    'edit own fact_doc content',
//    'mark ready for review',
    'view the administration theme',
  );
  user_role_grant_permissions($participating_teacher->rid, $participating_teacher_permissions);

  // Create the Portfolio Reader role
  $portfolio_reader = new stdClass();
  $portfolio_reader->name = 'portfolio reader';
  $portfolio_reader->weight = 3;
  user_role_save($portfolio_reader);

  $portfolio_reader_permissions = array(
    $filter_perms['filtered_html'],
//    'manage content workflow',
//    'approve and deny content',
    'view revisions',
    'access user profiles',
  );
  user_role_grant_permissions($portfolio_reader->rid, $portfolio_reader_permissions);

  // Create the Support Provider role
  $support_provider = new stdClass();
  $support_provider->name = 'support provider';
  $support_provider->weight = 4;
  user_role_save($support_provider);

  $support_provider_permissions = array(
    $filter_perms['filtered_html'],
    'view revisions',
    'access user profiles',
  );
  user_role_grant_permissions($support_provider->rid, $support_provider_permissions);

  // Create the induction administrator role
  $btsa_administrator = new stdClass();
  $btsa_administrator->name = 'induction administrator';
  $btsa_administrator->weight = 5;
  user_role_save($btsa_administrator);

  $btsa_administrator_permissions = array(
    $filter_perms['filtered_html'],
    'view revisions',
    'access user profiles',
  );
  user_role_grant_permissions($btsa_administrator->rid, $btsa_administrator_permissions);

    // Create the complete role. This is a role for teachers who have completed
    // the BTSA program
  $complete = new stdClass();
  $complete->name = 'complete';
  $complete->weight = 2;
  user_role_save($complete);

  $complete_permissions = array(
    $filter_perms['filtered_html'],
  );
  user_role_grant_permissions($complete->rid, $complete_permissions);


}

function _btsa_profile_setup_home_page() {
  // Need to revert so I can use the field_page_body field.
  features_revert(array('btsa_page' => array('field')));

  // Create the home page node.
  $node = new stdClass();
  $node->type = "page";
  $node->title = "Home";
  $node->language = LANGUAGE_NONE;
  $node->uid = 1;
  node_object_prepare($node);

  $node->field_page_body[LANGUAGE_NONE][0] = array(
    'value' => 'This is your homepage. If you are logged in you can click the edit link at the top to edit what is said here.',
    'format' => 'filtered_html',
  );

  $node = node_submit($node);
  node_save($node);

  // Save the front page.
  variable_set('site_frontpage', 'node/' . $node->nid);
}
