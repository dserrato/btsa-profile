
Create a site with this profile:
You can use the provided build file btsa-profile.build to build a complete drupal core and this profile with all the required modules. 

Simply run the following command.

$ drush make btsa-profile.build <target directory>

This will download Drupal core, all the contrib and custom modules.

The Make File: btsa_profile.make
All required modules are listed including their verision and any patches in the btsa_profile.make file.

Custom modules:

* BTSA_Rules:
Provides a Rules action so you can send an email to a user's
Reader when the action is triggered by your rule. One example would be when a 
fact doc is transitioned to a new state.

* BTSA Flow: 
Provides the workflow for factdocs. It uses and extends the 
State Machine API providing views integration. If you need to change the
workflow pages this is probably the module to start with

* BTSA Messaging: Provides a contact my Reader form at user/%user/email-reader 
and alters all system emails sent by the site using hook_mail_alter() 
providing a signature.

* btsa_features: A collection of feature modules
    * btsa_admin_info
    The profile assignment fields for Participating Teachers using Profile 2 Module.
    This feature primarily provides the btsa_admin_info profile2 type. You can see
    the fields it provides in it's .info file.

    * btsa_page
    Minor tweaks to the "Basic page" content type

    * btsa_pt_contexts
    Provides the contexts my_factdocs and pt_profile

    * btsa_pt_views
    Provides the views my_support_provider and pts_factdocs

    * btsa_reader_contexts
    Provides the contexts reader_pts and site_wide_reader_pts

    * btsa_reader_provider_views
    Provides the views readers and support_providers

    * btsa_reader_views
    Provides the view readers_assigned_pts2

    * btsa_support_provider_contexts
    Provides the context site_wide_support_providers_pts

    * btsa_support_provider_views
    Provides the view sp_assigned_pts

    * btsa_user_profile
    Provides profile2_type profile and all of it's fields. You can see all of the 
    fields in the module's .info file. It also provides the realname_pattern.

    * fact_doc
      * Provides the fact_doc content type and the form alters for it's node form
      * Provides the blocks for the dashboards, "Dashboard of Participating Teacher FactDocs for Readers" and "Dashboard of Participating Teacher FactDocs".
      * The fact_doc_log_messages view.
      * The following taxonomy vocabularies factdoc_module, factdoc_name, and factdoc_year.
      * Provides mapping of taxonomy terms with fact_doc_factdocterms 
