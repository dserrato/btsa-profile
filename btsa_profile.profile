<?php

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Allows the profile to alter the site configuration form.
 */
function btsa_profile_form_install_configure_form_alter(&$form, $form_state) {
  $form['site_information']['site_name']['#default_value'] = 'BTSA Profile';
  $form['site_information']['site_mail']['#default_value'] = 'dserrato@fcoe.org';
  $form['admin_account']['account']['name']['#default_value'] = 'admin';
  $form['admin_account']['account']['mail']['#default_value'] = 'dserrato@fcoe.org';
  $form['server_settings']['site_default_country']['#default_value'] = 'US';
}
