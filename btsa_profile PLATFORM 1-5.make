core = 7.x

api = 2
projects[drupal][version] = "7.22"

; Dependencies =================================================================

projects[auto_nodetitle][version] = 1.0
projects[auto_nodetitle][subdir] = "contrib"


projects[context][version] = 3.0-beta6
projects[context][subdir] = "contrib"

projects[ctools][version] = 1.3
projects[ctools][subdir] = "contrib"

projects[date][version] = 2.6
projects[date][subdir] = "contrib"

projects[diff][version] = 3.2
projects[diff][subdir] = "contrib"

projects[entity][version] = 1.1
projects[entity][subdir] = "contrib"

projects[entityreference][version] = 1.0
projects[entityreference][subdir] = "contrib"

projects[features][version] = 1.0
projects[features][subdir] = "contrib"

projects[field_permissions][version] = 1.0-beta2
projects[field_permissions][subdir] = "contrib"

projects[filefield_paths][version] = 1.0-beta4
projects[filefield_paths][subdir] = "contrib"

projects[libraries][version] = 1.0
projects[libraries][subdir] = "contrib"

projects[logintoboggan][version] = 1.3
projects[logintoboggan][subdir] = "contrib"

projects[pathauto][version] = 1.2
projects[pathauto][subdir] = "contrib"

projects[prepopulate][version] = 2.x-dev
projects[prepopulate][subdir] = "contrib"

projects[profile2][version] = 1.3
projects[profile2][subdir] = "contrib"

projects[realname][version] = 1.1
projects[realname][subdir] = "contrib"

projects[registration][version] = 1.1
projects[registration][subdir] = "contrib"

projects[rules][version] = 2.3
projects[rules][subdir] = "contrib"

projects[state_machine][version] = 2.1
projects[state_machine][subdir] = "contrib"

projects[strongarm][version] = 2.0
projects[strongarm][subdir] = "contrib"

projects[taxonomy_csv][version] = 5.10
projects[taxonomy_csv][subdir] = "contrib"

projects[token][version] = 1.5
projects[token][subdir] = "contrib"

projects[transliteration][version] = 3.1
projects[transliteration][subdir] = "contrib"

projects[views][version] = 3.7
projects[views][subdir] = "contrib"

projects[views_bulk_operations][version] = 3.0-rc1
projects[views_bulk_operations][subdir] = "contrib"





; Development
=================================================================

projects[coder][subdir] = "dev"

projects[devel][version] = 1.x-dev
projects[devel][subdir] = "dev"

projects[reroute_email][version] = 1.1
projects[reroute_email][subdir] = "dev"

; Custom Modules
=================================================================

projects[btsa_flow][type] = module
projects[btsa_flow][download][type] = git
projects[btsa_flow][download][url] = git@bitbucket.org:dserrato/btsa_flow.git
projects[btsa_flow][download][branch] = cleanup-extend
projects[btsa_flow][subdir] = custom

projects[btsa_rules][type] = module
projects[btsa_rules][download][type] = git
projects[btsa_rules][download][url] = git@bitbucket.org:dserrato/btsa_rules.git
projects[btsa_rules][download][branch] = master
projects[btsa_rules][subdir] = custom

projects[btsa_messaging][type] = module
projects[btsa_messaging][download][type] = git
projects[btsa_messaging][download][url] = git@bitbucket.org:dserrato/btsa_messaging.git
projects[btsa_messaging][download][branch] = master
projects[btsa_messaging][subdir] = custom



; Features =====================================================================

projects[btsa_features][type] = module
projects[btsa_features][download][type] = git
projects[btsa_features][download][url] = git@bitbucket.org:dserrato/btsa_features.git
projects[btsa_features][download][branch] = master
projects[btsa_features][subdir] = features
