core = 7.x

api = 2
projects[drupal][version] = "7.56"

; Dependencies =================================================================

projects[admin_menu][version] = 3.0-rc5
projects[admin_menu][subdir] = "contrib"

projects[auto_nodetitle][version] = 1.0
projects[auto_nodetitle][subdir] = "contrib"

projects[better_formats][version] = "1.0-beta2"
projects[better_formats][subdir] = "contrib"

projects[context][version] = 3.7
projects[context][subdir] = "contrib"

projects[context_profile_role][version] = 1.1
projects[context_profile_role][subdir] = "contrib"

projects[ctools][version] = 1.12
projects[ctools][subdir] = "contrib"

projects[date][version] = 2.10
projects[date][subdir] = "contrib"

projects[diff][version] = 3.3
projects[diff][subdir] = "contrib"

projects[editablefields][version] = "1.0-alpha3"
projects[editablefields][subdir] = "contrib"

projects[entity][version] = 1.8
projects[entity][subdir] = "contrib"

projects[entityreference][version] = 1.4
projects[entityreference][subdir] = "contrib"

projects[features][version] = 2.10
projects[features][subdir] = "contrib"

projects[field_group][version] = 1.5
projects[field_group][subdir] = "contrib"

projects[field_permissions][version] = 1.0
projects[field_permissions][subdir] = "contrib"

projects[filefield_paths][version] = 1.0
projects[filefield_paths][subdir] = "contrib"

projects[google_analytics][version] = 2.0
projects[google_analytics][subdir] = "contrib"

projects[jquery_update][version] = 2.7
projects[jquery_update][subdir] = "contrib"

projects[libraries][version] = 1.0
projects[libraries][subdir] = "contrib"

projects[link][version] = "1.4"
projects[link][subdir] = "contrib"

projects[logintoboggan][version] = 1.5
projects[logintoboggan][subdir] = "contrib"

projects[masquerade][version] = 1.0-rc7
projects[masquerade][subdir] = "contrib"

projects[module_filter][version] = "2.1"
projects[module_filter][subdir] = "contrib"

projects[nocurrent_pass][version] = "1.0"
projects[nocurrent_pass][subdir] = "contrib"

projects[pathauto][version] = 1.3
projects[pathauto][subdir] = "contrib"


; Leaving Prepopulate at 2.0 because 2.1 breaks fact doc submission functionality
projects[prepopulate][version] = 2.0
projects[prepopulate][subdir] = "contrib"

projects[profile2][version] = 1.3
projects[profile2][subdir] = "contrib"

projects[realname][version] = 1.3
projects[realname][subdir] = "contrib"

projects[registration][version] = 1.6
projects[registration][subdir] = "contrib"

projects[rules][version] = 2.10
projects[rules][subdir] = "contrib"

projects[special_menu_items][version] = 2.0
projects[special_menu_items][subdir] = "contrib"

projects[state_machine][version] = 2.5
projects[state_machine][subdir] = "contrib"

projects[strongarm][version] = 2.0
projects[strongarm][subdir] = "contrib"

projects[tabtamer][version] = 1.1
projects[tabtamer][subdir] = "contrib"

projects[taxonomy_csv][version] = 5.10
projects[taxonomy_csv][subdir] = "contrib"

projects[term_reference_filter_by_views][version] = 2.x-dev
projects[term_reference_filter_by_views][subdir] = "contrib"

projects[token][version] = 1.7
projects[token][subdir] = "contrib"

projects[transliteration][version] = 3.2
projects[transliteration][subdir] = "contrib"

projects[views][version] = 3.16
projects[views][subdir] = "contrib"

projects[views_bulk_operations][version] = 3.4
projects[views_bulk_operations][subdir] = "contrib"

projects[views_conditional][version] = 1.3
projects[views_conditional][subdir] = "contrib"

projects[views_data_export][version] = 3.2
projects[views_data_export][subdir] = "contrib"

projects[views_field_view][version] = 1.2
projects[views_field_view][subdir] = "contrib"

projects[views_php][version] = 1.0-alpha3
projects[views_php][subdir] = "contrib"

projects[views_role_based_global_text][version] = 1.1
projects[views_role_based_global_text][subdir]= "contrib"

projects[views_send][version] = 1.6
projects[views_send][subdir] = "contrib"

projects[wysiwyg][version] = 2.4
projects[wysiwyg][subdir] = "contrib"



; Development
=================================================================

projects[coder][subdir] = "dev"

projects[devel][version] = 1.x-dev
projects[devel][subdir] = "dev"

projects[reroute_email][version] = 1.1
projects[reroute_email][subdir] = "dev"

; Custom Modules
=================================================================

projects[btsa_flow][type] = module
projects[btsa_flow][download][type] = git
projects[btsa_flow][download][url] = git@bitbucket.org:dserrato/btsa_flow.git
projects[btsa_flow][download][branch] = cleanup-extend
projects[btsa_flow][subdir] = custom

projects[btsa_rules][type] = module
projects[btsa_rules][download][type] = git
projects[btsa_rules][download][url] = git@bitbucket.org:dserrato/btsa_rules.git
projects[btsa_rules][download][branch] = master
projects[btsa_rules][subdir] = custom

projects[btsa_messaging][type] = module
projects[btsa_messaging][download][type] = git
projects[btsa_messaging][download][url] = git@bitbucket.org:dserrato/btsa_messaging.git
projects[btsa_messaging][download][branch] = master
projects[btsa_messaging][subdir] = custom

projects[schoolyearlist][type] = module
projects[schoolyearlist][download][type] = git
projects[schoolyearlist][download][url] = git@bitbucket.org:dserrato/schoolyearlist.git
projects[schoolyearlist][download][branch] = master
projects[schoolyearlist][subdir] = custom

projects[updaterealname][type] = module
projects[updaterealname][download][type] = git
projects[updaterealname][download][url] = git@bitbucket.org:fcoe-webservices/update-realname.git
projects[updaterealname][download][branch] = master
projects[updaterealname][subdir] = custom

projects[reg_cancel][type] = module
projects[reg_cancel][download][type] = git
projects[reg_cancel][download][url] = git@bitbucket.org:fcoe-webservices/registration-cancel-override.git
projects[reg_cancel][download][branch] = master
projects[reg_cancel][directory_name] = reg_cancel
projects[reg_cancel][subdir] = custom

; Features =====================================================================

projects[btsa_features][type] = module
projects[btsa_features][download][type] = git
projects[btsa_features][download][url] = git@bitbucket.org:dserrato/btsa_features.git
projects[btsa_features][download][branch] = master
projects[btsa_features][subdir] = features

; Themes 
=================================================================

projects[induction][download][type] = "git"
projects[induction][download][url] = "git@bitbucket.org:fcoe-webservices/induction-theme.git"
projects[induction][directory_name] = "induction"
projects[induction][type] = "theme"

projects[induction_whiplash][download][type] = "git"
projects[induction_whiplash][download][url] = "git@bitbucket.org:fcoe-webservices/induction-whiplash.git"
projects[induction_whiplash][directory_name] = "induction_whiplash"
projects[induction_whiplash][type] = "theme"
